import com.hamrasta.hplatform.asset.Address;
import org.apache.commons.codec.DecoderException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class AddressTest {
    // private: 50e13abe204cb6355c5268e12fe622b5c2888349c7319eaa1fbaf1b485fd1b5c
    // public : 4ac4c0bafddc7f512967451f8ef84f81c58ab5cc2ab3e9a70536f26f3cb4bec980e8e63ef00945f6877935bd37e8e280a3b3511f520630b20954c33ad409ad8b
    // readable: WAebpdyRF14Ry9BcnzQ3mBtPSpHMwccf4b
    @Test
    public void addressGenerate() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, DecoderException {
        Address address = Address.generate(true, 3);
        Assertions.assertTrue(Address.validateAddress(true, address.getReadableAddress()));
        Assertions.assertFalse(Address.validateAddress(false, address.getReadableAddress()));
        Address ad = Address.load(true, address.getPrivateKeys());
        Assertions.assertEquals(ad, address);

    }
    @Test
    public void mnemonic() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, DecoderException {
        Address address = Address.generate(false);
        Address address2 = Address.loadFromMnemonic(false, address.mnemonic());
        Assertions.assertEquals(address, address2);
        address = Address.generate(false, 3);
        address2 = Address.loadFromMnemonic(false, address.mnemonic());
        Assertions.assertNotEquals(address, address2);
        address2 = Address.loadFromMnemonic(false, address.mnemonics());
        Assertions.assertEquals(address, address2);
    }

    @Test
    public void multiplSignature() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, DecoderException {
        Address address = Address.generate(false, 3);
        Address address2 = Address.loadAddressOnly(false, address.getPublicKeys());
        Assertions.assertEquals(address.getReadableAddress(), address2.getReadableAddress());
        Assertions.assertNotEquals(address, address2);
    }
}
