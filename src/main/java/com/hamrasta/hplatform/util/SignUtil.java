package com.hamrasta.hplatform.util;

import com.hamrasta.hplatform.asset.Address;
import com.hamrasta.hplatform.request.RPCRequest;
import org.web3j.crypto.Sign;

import java.math.BigInteger;
import java.security.SignatureException;
import java.util.LinkedList;
import java.util.List;

public class SignUtil {
    public static String extractAddress(RPCRequest request, boolean testnet) {
        if (request == null) {
            throw new RuntimeException("request is null, bad hex");
        }
        try {
            List<BigInteger> publicKeys = new LinkedList<>();
            for (int i =0; i < request.getSignatures().length; i ++) {
                publicKeys.add(Sign.signedMessageToKey(request.getRawPart(), request.getSignatures()[i]));
            }
            // retrieve address from signature
            return Address.computeReadableAddress(testnet, publicKeys.toArray(new BigInteger[0]));
        } catch (SignatureException e) {
            throw new RuntimeException("signature is not valid");
        }
    }
}
