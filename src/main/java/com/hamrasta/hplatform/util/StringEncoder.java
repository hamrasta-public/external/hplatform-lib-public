package com.hamrasta.hplatform.util;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static com.hamrasta.hplatform.util.ByteUtils.readNBytes;

public class StringEncoder {
    public static void optionalEncode(ByteArrayOutputStream bos, String value, int len, boolean hex, boolean address) throws IOException, DecoderException {
        if (value == null || value.isEmpty())
        {
            bos.write((byte)0);
        } else {
            bos.write((byte)255);
            encode(bos, value, len, hex, address);
        }
    }

    public static void encode(ByteArrayOutputStream bos, String value, int len, boolean hex, boolean address) throws IOException,
            DecoderException {
        value = value.trim();
        if (address) {
            bos.write(Base58.decode(value));
        } else {
            if (len > 0)
                if (!hex)
                    bos.write(StringUtils.leftPad(value.substring(0, Math.min(len, value.length())), len, " ").getBytes(StandardCharsets.US_ASCII));
                else
                    bos.write(Hex.decodeHex(value));
            else {
                byte[] d;
                if (hex) {
                    d = Hex.decodeHex(value);
                } else {
                    d = value.getBytes(StandardCharsets.UTF_8);
                }
                bos.write(ByteUtils.intToBytes(d.length));
                bos.write(d);
            }
        }
    }

    public static String optionalDecode(ByteArrayInputStream bis, int len, boolean hex, boolean address) throws IOException {
        String value = "";
        if (bis.read() != 0) {
            value = decode(bis, len, hex, address);
        }
        return value;
    }

    public static String decode(ByteArrayInputStream bis, int len, boolean hex, boolean address) throws IOException {
        if (address) {
            return Base58.encode(readNBytes(bis, 25));
        } else {
            String value = "";
            if (len > 0) {
                if (hex) {
                    value = Hex.encodeHexString(readNBytes(bis, len));
                } else {
                    value = new String(readNBytes(bis, len), StandardCharsets.US_ASCII).trim();
                }
            } else {
                int l = ByteUtils.bytesToInt(readNBytes(bis, 4));
                if (hex) {
                    value = Hex.encodeHexString(readNBytes(bis, l));
                } else {
                    value = new String(readNBytes(bis, l), StandardCharsets.UTF_8);
                }

            }
            return value;
        }
    }

    public static void optionalEncode(ByteArrayOutputStream bos, String name, int i) throws IOException, DecoderException {
        optionalEncode(bos, name, i, false, false);
    }
    public static String optionalDecode(ByteArrayInputStream bis, int i) throws IOException {
        return optionalDecode(bis, i, false, false);
    }
}
