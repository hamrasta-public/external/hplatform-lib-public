package com.hamrasta.hplatform.util;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.web3j.crypto.Sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ByteUtils {


    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(0, x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes, 0, bytes.length);
        ((Buffer)buffer).flip();//need flip
        return buffer.getLong();
    }

    public static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(0, x);
        return buffer.array();
    }

    public static int bytesToInt(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes, 0, bytes.length);
        ((Buffer)buffer).flip();//need flip
        return buffer.getInt();
    }

    public static byte[][] sort(byte[][] data) {
        List<String> hexs = new LinkedList<>();
        for (int i = 0; i < data.length; i++) {
            hexs.add(Hex.encodeHexString(data[i]));
        }
        Collections.sort(hexs);
        byte[][] res = new byte[data.length][];
        for (int i = 0; i < hexs.size(); i++) {
            try {
                res[i] = Hex.decodeHex(hexs.get(i));
            } catch (DecoderException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    public static Sign.SignatureData[] readSignaturesFromStream(ByteArrayInputStream bis) throws IOException {
        int numSigns = ByteUtils.bytesToInt(readNBytes(bis,4));
        List<Sign.SignatureData> list = new LinkedList<>();
        for (int i = 0; i < numSigns; i++)
            list.add(new Sign.SignatureData(readNBytes(bis,1), readNBytes(bis,32), readNBytes(bis,32)));
        return list.toArray(new Sign.SignatureData[0]);
    }

    public static void writeSignaturesToStream(ByteArrayOutputStream bos, Sign.SignatureData[] signatures) throws IOException {
        if (signatures == null)
        {
            bos.write(new byte[4]);
            bos.write(new byte[1]);
            bos.write(new byte[32]);
            bos.write(new byte[32]);
        } else {
            bos.write(ByteUtils.intToBytes(signatures.length));
            for (int i = 0; i < signatures.length; i ++) {
                bos.write(signatures[i].getV());
                bos.write(signatures[i].getR());
                bos.write(signatures[i].getS());
            }

        }
    }

    public static byte[] readNBytes(InputStream bis, int len) throws IOException {
        int n = 0;
        byte[] b = new byte[len];
        while (n < len) {
            int count = bis.read(b, n, len - n);
            if (count < 0)
                break;
            n += count;
        }
        return b;
    }
}

