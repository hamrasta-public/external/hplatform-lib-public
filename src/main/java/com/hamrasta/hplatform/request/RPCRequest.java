package com.hamrasta.hplatform.request;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;
import com.hamrasta.hplatform.serializer.Marshal;
import com.hamrasta.hplatform.util.ByteUtils;
import com.hamrasta.hplatform.util.StringEncoder;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class RPCRequest implements IAsset {
    @AssetField(index = 10001)
    protected Sign.SignatureData[] signatures;
    @AssetField(index = 10000)
    protected long nonce;
    @AssetField(index = -1, length = 32, hex = false, maxLength = 32, optional = true)
    protected String name;


    public RPCRequest() {

    }

    protected RPCRequest(String name) {
        this.name = name;
    }

    public RPCRequest(long nonce) {
        this.nonce = nonce;
    }

    public RPCRequest(String name, long nonce) {
        this(name);
        this.nonce = nonce;
    }

    public RPCRequest(String name, Sign.SignatureData[] signatures, long nonce) {
        this(name, nonce);
        this.signatures = signatures;
    }

    public RPCRequest(Sign.SignatureData[] signatures, long nonce) {
        this(nonce);
        this.signatures = signatures;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getRawPart() {
        byte[] array = Marshal.pack(this);
        int l = signatures == null ? 1 : signatures.length; // if signature is null encode will write a dummy 1 signature
        byte[] res = new byte[array.length - (65 * l) - 4];
        System.arraycopy(array, 0, res, 0, array.length - (65 * l) - 4);
        return res;
    }

    public Sign.SignatureData[] getSignatures() {
        return signatures;
    }

    public void setSignatures(Sign.SignatureData[] signatures) {
        this.signatures = signatures;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }


    public RPCRequest sign(ECKeyPair ecKeyPair) {
        byte[] array = getRawPart();
        if (getSignatures() == null)
            setSignatures(new Sign.SignatureData[] {Sign.signMessage(array, ecKeyPair)});
        else {
            Set<Sign.SignatureData> list = new HashSet<>(Arrays.asList(signatures));
            list.add(Sign.signMessage(array, ecKeyPair));
            setSignatures(list.toArray(new Sign.SignatureData[0]));
        }
        return this;
    }

    public RPCRequest sign(String... privateKeyHex) {
        try {
            for (String key : privateKeyHex)
                sign(ECKeyPair.create(Hex.decodeHex(key)));
            return this;
        } catch (DecoderException ex) {
            ex.printStackTrace();
        }
        return this;
    }


}
