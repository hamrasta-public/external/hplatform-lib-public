package com.hamrasta.hplatform.serializer;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AssetField {
    public boolean optional() default false;
    public int maxLength() default 0;
    public int length() default 0;
    public boolean address() default false;
    public boolean hex() default true;
    public int index();
    public String customEncoder() default "";
    public String customDecoder() default "";
    public String ifTrue() default "";
    public boolean checkEmpty() default false;


}
