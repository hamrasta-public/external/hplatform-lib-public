package com.hamrasta.hplatform.serializer;

import com.google.common.base.Defaults;
import com.hamrasta.hplatform.util.ByteUtils;
import com.hamrasta.hplatform.util.StringEncoder;
import org.web3j.crypto.Sign;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class Marshal {

    private static <T> T unpack(Class<T> clazz, ByteArrayInputStream bis) {
        try {
            if (!IAsset.class.isAssignableFrom(clazz))
                throw new RuntimeException("This class is not Implementing IAsset " + clazz.getName());

            T object = clazz.newInstance();
            List<Field> fields = new LinkedList<>();
            Class<?> p = clazz;
            while (p != null && p != Object.class) {
                for (Field field : p.getDeclaredFields()) {
                    if (field.isAnnotationPresent(AssetField.class)) {
                        field.setAccessible(true);
                        fields.add(field);
                    }
                }
                p = p.getSuperclass();
            }

            fields.sort((o1, o2) -> {
                return Integer.compare(o1.getAnnotation(AssetField.class).index(),
                        o2.getAnnotation(AssetField.class).index());
            });

            for (int i = 0; i < fields.size(); i++) {
                if (IAsset.class.isAssignableFrom(fields.get(i).getType())) {
                    fields.get(i).set(object, unpack(fields.get(i).getType(), bis));
                } else if (fields.get(i).getType().isArray()) {
                    if (fields.get(i).getType() == Sign.SignatureData[].class) {
                        fields.get(i).set(object, ByteUtils.readSignaturesFromStream(bis));
                    } else {
                        int size = ByteUtils.bytesToInt(ByteUtils.readNBytes(bis, 4));
                        Object array = Array.newInstance(fields.get(i).getType().getComponentType(), size);
                        for (int j = 0; j < size; j++) {

                            if (IAsset.class.isAssignableFrom(fields.get(i).getType().getComponentType()))
                                Array.set(array, j, unpack(fields.get(i).getType().getComponentType(), bis));
                            else
                                Array.set(array, j, unpackField(clazz, fields.get(i), object, fields.get(i).getType().getComponentType(), bis));
                        }
                        fields.get(i).set(object, array);
                    }
                } else {
                    fields.get(i).set(object, unpackField(clazz, fields.get(i), object, fields.get(i).getType(), bis));

                }
            }


            return object;
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static byte[] pack(IAsset object) {
        try {
            Class<?> clazz = object.getClass();
            List<Field> fields = new LinkedList<>();
            Class<?> p = clazz;
            while (p != null && p != Object.class) {
                for (Field field : p.getDeclaredFields()) {
                    if (field.isAnnotationPresent(AssetField.class)) {
                        field.setAccessible(true);
                        fields.add(field);
                    }
                }
                p = p.getSuperclass();
            }

            fields.sort((o1, o2) -> {
                return Integer.compare(o1.getAnnotation(AssetField.class).index(),
                        o2.getAnnotation(AssetField.class).index());
            });
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (int i = 0; i < fields.size(); i++) {
                Object obj = fields.get(i).get(object);
                if (obj instanceof IAsset) {
                    bos.write(pack((IAsset) obj));
                } else if (fields.get(i).getType().isArray()) {
                    if (fields.get(i).getType() == Sign.SignatureData[].class) {
                        ByteUtils.writeSignaturesToStream(bos, (Sign.SignatureData[]) obj);
                    } else {
                        int size = obj == null ? 0 : Array.getLength(obj);
                        bos.write(ByteUtils.intToBytes(size));
                        for (int j = 0; j < size; j++) {
                            Object it = Array.get(obj, j);
                            if (it instanceof IAsset)
                                bos.write(pack((IAsset) it));
                            else
                                packField(clazz, fields.get(i), object, it, bos);
                        }
                    }
                } else {
                    packField(clazz, fields.get(i), object, obj, bos);
                }
            }

            bos.close();
            return bos.toByteArray();
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static Object unpackField(Class<?> clazz, Field field, Object parent, Class<?> fieldType, ByteArrayInputStream bis) throws Exception {
        AssetField assetField = field.getAnnotation(AssetField.class);
        if (!assetField.ifTrue().isEmpty()) {
            String ifTrue = assetField.ifTrue();
            if (assetField.ifTrue().startsWith("!"))
                ifTrue = ifTrue.substring(1);
            Field f = clazz.getDeclaredField(ifTrue);
            f.setAccessible(true);
            Object val = f.get(parent);
            if (val instanceof Boolean) {
                if ((!(boolean) val && !assetField.ifTrue().startsWith("!")) || ((boolean) val && assetField.ifTrue().startsWith("!")) )
                    if (fieldType == String.class)
                        return "";
                    else if (!fieldType.isPrimitive())
                        return fieldType.newInstance();
                    else return Defaults.defaultValue(fieldType);
            } else if (val instanceof Integer) {
                if (((int)val <= 0 && !assetField.ifTrue().startsWith("!")) || ((int)val > 0 && assetField.ifTrue().startsWith("!")))
                    if (fieldType == String.class)
                        return "";
                    else if (!fieldType.isPrimitive())
                        return fieldType.newInstance();
                    else return Defaults.defaultValue(fieldType);
            }
        }
        if (!assetField.customDecoder().isEmpty()) {
            Method method = clazz.getDeclaredMethod(assetField.customDecoder(), ByteArrayInputStream.class);
            method.setAccessible(true);
            return method.invoke(parent, bis);
        } else {
            if (fieldType == String.class) {
                String val = "";

                if (assetField.optional()) {
                    val = StringEncoder.optionalDecode(bis, assetField.length(), assetField.hex(), assetField.address());
                } else {
                    val = StringEncoder.decode(bis, assetField.length(), assetField.hex(),
                            assetField.address());
                }
                return val;
            } else if (fieldType == Boolean.class || field.getType() == boolean.class) {
                return (bis.read() != 0);
            } else if (fieldType == Integer.class || field.getType() == int.class) {
                return ByteUtils.bytesToInt(ByteUtils.readNBytes(bis, 4));
            } else if (fieldType == Long.class || field.getType() == long.class) {
                return ByteUtils.bytesToLong(ByteUtils.readNBytes(bis, 8));
            } else if (fieldType == Byte.class || field.getType() == byte.class) {
                return (byte)bis.read();
            } else if (fieldType == Sign.SignatureData[].class) {
                return ByteUtils.readSignaturesFromStream(bis);
            } else {
                throw new RuntimeException("Could not serialize this filed " + field.getName() + " " + field.toString());
            }
        }

    }

    private static void packField(Class<?> clazz, Field field, Object parent, Object obj,
                                  ByteArrayOutputStream bos) throws Exception {
        AssetField assetField = field.getAnnotation(AssetField.class);
        if (!assetField.ifTrue().isEmpty()) {
            String ifTrue = assetField.ifTrue();
            if (assetField.ifTrue().startsWith("!"))
                ifTrue = ifTrue.substring(1);
            Field f = clazz.getDeclaredField(ifTrue);
            f.setAccessible(true);
            Object val = f.get(parent);
            if (val instanceof Boolean) {
                if ((!(boolean) val && !assetField.ifTrue().startsWith("!")) || ((boolean) val && assetField.ifTrue().startsWith("!")) )
                    return;
            } else if (val instanceof Integer) {
                if (((int)val <= 0 && !assetField.ifTrue().startsWith("!")) || ((int)val > 0 && assetField.ifTrue().startsWith("!")))
                    return;
            }
        }
        if (!assetField.customEncoder().isEmpty()) {
            Method method = clazz.getDeclaredMethod(assetField.customEncoder(), ByteArrayOutputStream.class);
            method.setAccessible(true);
            method.invoke(parent, bos);
        } else {
            Class<?> fieldType = field.getType().isArray() ? field.getType().getComponentType() : field.getType();
            if (fieldType == String.class) {
                String val = obj == null ? "" : (String) obj;
                if (assetField.checkEmpty() && val.isEmpty())
                    return;
                if (assetField.maxLength() > 0 && val.length() > assetField.maxLength())
                    val = val.substring(0, Math.min(val.length(), assetField.maxLength()));
                if (assetField.optional()) {
                    StringEncoder.optionalEncode(bos, val, assetField.length(), assetField.hex(), assetField.address());
                } else {
                    StringEncoder.encode(bos, val, assetField.length(), assetField.hex(),
                            assetField.address());
                }
            } else if (fieldType == Boolean.class || fieldType == boolean.class) {
                if (obj == null)
                    obj = false;
                if ((Boolean) obj)
                    bos.write((byte) 255);
                else bos.write((byte) 0);
            } else if (fieldType == Integer.class || fieldType == int.class) {
                if (obj == null)
                    obj = 0;
                bos.write(ByteUtils.intToBytes((Integer) obj));
            } else if (fieldType == Long.class || fieldType == long.class) {
                if (obj == null)
                    obj = 0L;
                bos.write(ByteUtils.longToBytes((Long) obj));
            } else if (fieldType == Byte.class || fieldType == byte.class) {
                if (obj == null)
                    obj = 0;
                bos.write((byte) obj);
            } else if (fieldType == Sign.SignatureData[].class) {
                ByteUtils.writeSignaturesToStream(bos, (Sign.SignatureData[]) obj);
            } else {
                throw new RuntimeException("Could not serialize this filed " + field.getName() + " " + field.toString());
            }
        }
    }

    public static <T> T unpack(Class<T> clazz, byte[] data, boolean nullValue) {
        try {
            if (!IAsset.class.isAssignableFrom(clazz))
                throw new RuntimeException("This class is not Implementing IAsset " + clazz.getName());
            if (data == null || data.length == 0)
                if (nullValue)
                    return null;
                else return (T) ((IAsset) clazz.newInstance()).defaultValue();
            return unpack(clazz, new ByteArrayInputStream(data));
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static <T> T unpack(Class<T> clazz, byte... data) {
        return unpack(clazz, data, false);
    }

}
