package com.hamrasta.hplatform.serializer;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


public interface IAsset {
    default IAsset defaultValue() {
        return null;
    }
}
