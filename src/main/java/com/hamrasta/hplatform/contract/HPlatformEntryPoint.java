package com.hamrasta.hplatform.contract;

import com.google.protobuf.ByteString;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ResponseUtils;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class HPlatformEntryPoint extends ChaincodeBase {
    private static Log _logger = LogFactory.getLog(HPlatformEntryPoint.class);
    private HPlatformContract hPlatformContract;
    private boolean testnet;
    public HPlatformEntryPoint(HPlatformContract hPlatformContract, boolean testnet) {
        this.hPlatformContract = hPlatformContract;
        this.testnet = testnet;
    }

    @Override
    public void start(String[] args) {
        super.start(args);
    }

    @Override
    public Response init(ChaincodeStub stub) {
        return hPlatformContract.initJob(stub, testnet);

    }

    @Override
    public Response invoke(ChaincodeStub stub) {
        try {
            _logger.info("Invoke contract with function " + stub.getFunction());
            String func = stub.getFunction();
            List<String> params = stub.getParameters();
            _logger.info("params -> " + String.join(",", params));
            String res = hPlatformContract.invoke(hPlatformContract.createContext(stub).getStub(), func, params);
            return ResponseUtils.newSuccessResponse(ByteString.copyFrom(res, StandardCharsets.UTF_8).toByteArray());
        } catch (Throwable e) {
            return ResponseUtils.newErrorResponse(e.getMessage());
        }

    }
}
