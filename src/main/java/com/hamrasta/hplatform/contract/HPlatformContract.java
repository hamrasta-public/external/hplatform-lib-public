package com.hamrasta.hplatform.contract;

import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.Collection;

public abstract class HPlatformContract implements ContractInterface {

    public abstract Chaincode.Response init(ChaincodeStub stub, boolean testnet);

    public abstract String invoke(ChaincodeStub stub, String func, Collection<String> params) throws Throwable;


    public Chaincode.Response initJob(ChaincodeStub stub, boolean testnet) {
        stub.putStringState("token_network_testnet", String.valueOf(testnet));
        return init(stub, testnet);
    }
}
