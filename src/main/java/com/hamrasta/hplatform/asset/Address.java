package com.hamrasta.hplatform.asset;




import com.hamrasta.hplatform.util.Base58;
import com.hamrasta.hplatform.util.ByteUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Hash;
import org.web3j.crypto.Keys;
import org.web3j.crypto.MnemonicUtils;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.Objects;

public class Address {

    private String[] publicKeys;
    private String[] privateKeys;
    private String addressHex;
    private String readableAddress;
    private String hash;


    public Address(boolean testnet, byte[][] publicKeys, byte[][] privateKeys) {
        hash = "";
        publicKeys = ByteUtils.sort(publicKeys);
        this.publicKeys = new String[publicKeys.length];
        for (int i = 0; i < publicKeys.length; i++) {
            this.publicKeys[i] = Hex.encodeHexString(publicKeys[i]);
            hash = Hash.sha3(hash + this.publicKeys[i]);
        }
        privateKeys = ByteUtils.sort(privateKeys);
        this.privateKeys = new String[privateKeys.length];

        for (int i = 0; i < privateKeys.length; i++) {
            this.privateKeys[i] = Hex.encodeHexString(privateKeys[i]);
            hash = Hash.sha3(hash + this.privateKeys[i]);
        }
        this.addressHex = computeAddress(testnet, publicKeys);
        this.readableAddress = computeReadableAddress(addressHex);
    }

    public String getPublicKey() {
        return publicKeys.length > 0 ? publicKeys[0] : "";
    }

    public String getPrivateKey() {
        return privateKeys.length > 0 ? privateKeys[0] : "";
    }

    public String[] getPublicKeys() {
        return publicKeys;
    }

    public void setPublicKeys(String[] publicKeys) {
        this.publicKeys = publicKeys;
    }

    public String[] getPrivateKeys() {
        return privateKeys;
    }

    public void setPrivateKeys(String[] privateKeys) {
        this.privateKeys = privateKeys;
    }

    public String getAddressHex() {
        return addressHex;
    }

    public void setAddressHex(String addressHex) {
        this.addressHex = addressHex;
    }

    public String getReadableAddress() {
        return readableAddress;
    }

    public void setReadableAddress(String readableAddress) {
        this.readableAddress = readableAddress;
    }

    @Override
    public String toString() {
        return "Address{" +
                "addressHex='" + addressHex + '\'' +
                ", readableAddress='" + readableAddress + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return hash.equals(address.hash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash);
    }

    public static Address generate(boolean testnet) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        return generate(testnet, 1);
    }

    public static Address generate(boolean testnet, int numberOfSigners) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
        byte[][] pubs = new byte[numberOfSigners][];
        byte[][] privs = new byte[numberOfSigners][];
        for (int i = 0 ; i < numberOfSigners; i++) {
            ECKeyPair pair = Keys.createEcKeyPair();
            while (pair.getPrivateKey().toByteArray().length != 32)
                pair = Keys.createEcKeyPair();
            pubs[i] = pair.getPublicKey().toByteArray();
            privs[i] = pair.getPrivateKey().toByteArray();
        }

        return new Address(testnet, pubs, privs);
    }

    public static Address load(boolean testnet, String[] privateKeys, String[] publicKeys) throws DecoderException {
        byte[][] pubs = new byte[publicKeys.length][];
        byte[][] privs = new byte[privateKeys.length][];
        for (int i = 0; i < privateKeys.length; i++) {
            privs[i] = Hex.decodeHex(privateKeys[i]);
        }
        for (int i = 0; i < publicKeys.length; i++) {
            pubs[i] = Hex.decodeHex(publicKeys[i]);
        }
        return new Address(testnet, pubs, privs);
    }

    public static Address load(boolean testnet, String... privateKeys) throws DecoderException {
        byte[][] pubs = new byte[privateKeys.length][];
        byte[][] privs = new byte[privateKeys.length][];
        for (int i = 0; i < privateKeys.length; i++) {
            ECKeyPair pair = ECKeyPair.create(Hex.decodeHex(privateKeys[i]));
            privs[i] = pair.getPrivateKey().toByteArray();
            pubs[i] = pair.getPublicKey().toByteArray();
        }
        return new Address(testnet, pubs, privs);
    }

    public static Address loadAddressOnly(boolean testnet, String... publicKeys) throws DecoderException {
        return load(testnet, new String[]{}, publicKeys);
    }

    public static Address loadFromMnemonic(boolean testnet, String... mnemonics) throws DecoderException {
        String[] privateKeys = new String[mnemonics.length];
        for (int i = 0; i < mnemonics.length; i++)
            privateKeys[i] = Hex.encodeHexString(MnemonicUtils.generateEntropy(mnemonics[i]));
        return load(testnet, privateKeys);
    }

    public static String computeAddress(boolean testnet, byte[]... publicKeys) {
        try {
            publicKeys = ByteUtils.sort(publicKeys);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            for (int i = 0; i < publicKeys.length; i++)
                bos.write(publicKeys[i]);
            bos.close();
            byte[] addr = Hash.sha3(bos.toByteArray());
            byte[] res = new byte[21];
            System.arraycopy(addr, 12, res, 1, 20);
            res[0] = testnet ? (byte) 0x54 : 0x48;
            return Hex.encodeHexString(res);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String computeAddress(boolean testnet, String... publicKeyHexs) throws DecoderException {
        byte[][] data = new byte[publicKeyHexs.length][];
        for (int i = 0 ; i < publicKeyHexs.length ; i ++)
            data[i] = Hex.decodeHex(publicKeyHexs[i]);
        return computeAddress(testnet, data);
    }

    public static boolean validateAddress(boolean testnet, String address) {
        byte[] arr = Base58.decode(address);
        if (arr.length != 25)
            return false;
        if (testnet && arr[0] != 0x54)
            return false;
        if (!testnet && arr[0] != 0x48)
            return false;
        byte[] res = new byte[21];
        System.arraycopy(arr, 0, res, 0, 21);
        byte[] checksum = new byte[4];
        System.arraycopy(arr, 21, checksum, 0, 4);
        byte[] dh = Hash.sha256(Hash.sha256(res));
        byte[] checksum2 = new byte[4];
        System.arraycopy(dh, 0, checksum2, 0, 4);
        return Arrays.equals(checksum, checksum2);
    }

    public static String computeReadableAddress(String addressHex) {
        try {
            byte[] address = Hex.decodeHex(addressHex);
            byte[] sha256 = Hash.sha256(address);
            byte[] sha2562 = Hash.sha256(sha256);
            byte[] checksum = new byte[4];
            System.arraycopy(sha2562, 0, checksum, 0, 4);
            byte[] addressChecksum = new byte[address.length + 4];
            System.arraycopy(address, 0, addressChecksum, 0, address.length);
            System.arraycopy(checksum, 0, addressChecksum, address.length, 4);
            return Base58.encode(addressChecksum);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String computeReadableAddress(boolean testnet, BigInteger publicKey) {
        return computeReadableAddress(computeAddress(testnet, new byte[][] {publicKey.toByteArray()}));
    }
    public static String computeReadableAddress(boolean testnet, BigInteger... publicKeys) {
        byte[][] data = new byte[publicKeys.length][];
        for (int i = 0 ; i < publicKeys.length ; i ++)
            data[i] = publicKeys[i].toByteArray();
        return computeReadableAddress(computeAddress(testnet, data));
    }

    public String[] mnemonics() {
        try {
            String[] result = new String[privateKeys.length];
            for (int i = 0; i < privateKeys.length; i++)
                result[i] = MnemonicUtils.generateMnemonic(Hex.decodeHex(privateKeys[i]));
            return result;
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String mnemonic() {
        String[] res = mnemonics();
        return res.length > 0 ? res[0] : "";
    }
}
