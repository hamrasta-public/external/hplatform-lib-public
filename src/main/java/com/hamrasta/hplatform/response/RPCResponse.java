package com.hamrasta.hplatform.response;

import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class RPCResponse implements IAsset {
    @AssetField(index = 1000, length = 32, checkEmpty = true)
    protected String txId;

    protected RPCResponse() {
        this.txId = "";
    }

    public RPCResponse(String txId) {
        this.txId = txId;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }



}
