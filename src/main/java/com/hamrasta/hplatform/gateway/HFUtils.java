package com.hamrasta.hplatform.gateway;

import org.hyperledger.fabric.gateway.Identities;
import org.hyperledger.fabric.gateway.Identity;
import org.hyperledger.fabric.gateway.Wallet;
import org.hyperledger.fabric.gateway.X509Identity;
import org.hyperledger.fabric.gateway.impl.identity.GatewayUser;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.identity.X509Enrollment;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric.sdk.security.CryptoSuiteFactory;
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.hyperledger.fabric_ca.sdk.exception.EnrollmentException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.CertificateException;
import java.util.Properties;
import java.util.UUID;

public class HFUtils {
    static HFCAClient caClient;

    public static HFCAClient getHfcaClient(boolean testnet) throws IOException, CryptoException,
            InvalidArgumentException, ClassNotFoundException, IllegalAccessException, InstantiationException,
            NoSuchMethodException, InvocationTargetException {
        if (caClient == null) {

            // Create a CA client for interacting with the CA.
            byte[] bytes = Files.readAllBytes(testnet ? Paths.get("keyfiles-testnet/peerOrganizations/testnet" +
                    ".hamrasta.com/ca/ca.testnet.hamrasta.com-cert.pem") : Paths.get("keyfiles-mainnet" +
                    "/peerOrganizations/n1.hamrasta.com/ca/ca.n1.hamrasta.com-cert.pem"));
            Properties props = new Properties();
            props.put("pemBytes", bytes);
            props.put("allowAllHostNames", "true");
            caClient = HFCAClient.createNewInstance(!testnet ? "https://37.152.176.144:7000" : "https://37.152.180.255:7000", props);
            CryptoSuite cryptoSuite = CryptoSuiteFactory.getDefault().getCryptoSuite();
            caClient.setCryptoSuite(cryptoSuite);
            return caClient;

        } else {
            return caClient;
        }

    }

    public static Identity getAdmin(Wallet wallet, boolean testnet) throws IllegalAccessException,
            InstantiationException,
            InvocationTargetException, NoSuchMethodException, IOException, CryptoException, InvalidArgumentException,
            ClassNotFoundException, EnrollmentException,
            org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException, CertificateException {
        if (wallet.get("admin") == null) {
            EnrollmentRequest request = new EnrollmentRequest();
            request.setProfile("tls");
            request.addHost(!testnet ? "https://37.152.176.144:7000" : "https://37.152.180.255:7000");
            HFCAClient caClient = getHfcaClient(testnet);
            Enrollment enrollment = caClient.enroll("admin", "adminpw", request);
            Identity admin = Identities.newX509Identity(testnet ? "testnet-hamrasta-com" : "n1-hamrasta-com", enrollment);
            wallet.put("admin", admin);
        }
        return wallet.get("admin");
    }

    public static Identity getAppUser(Wallet wallet, boolean testnet) throws Exception {
        if (wallet.get("appUser") == null) {
            HFCAClient caClient = getHfcaClient(testnet);
            String userId = UUID.randomUUID().toString();
            RegistrationRequest registrationRequest = new RegistrationRequest(userId);
            X509Identity admin = (X509Identity) getAdmin(wallet, testnet);
            Enrollment adminEnrollment = new X509Enrollment(admin.getPrivateKey(),
                    Identities.toPemString(admin.getCertificate()));
            String enrollmentSecret = caClient.register(registrationRequest, new GatewayUser("admin", (testnet ? "testnet" : "n1") +
                    "-hamrasta-com", adminEnrollment));
            Enrollment enrollment = caClient.enroll(userId, enrollmentSecret);
            Identity user = Identities.newX509Identity(testnet ? "testnet-hamrasta-com" : "n1-hamrasta-com", enrollment);
            wallet.put("appUser", user);
        }
        return wallet.get("appUser");
    }
}
