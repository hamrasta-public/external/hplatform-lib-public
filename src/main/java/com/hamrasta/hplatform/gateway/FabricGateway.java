package com.hamrasta.hplatform.gateway;

import org.hyperledger.fabric.gateway.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class FabricGateway {
    private static Wallet wallet;
    private static Gateway gateway;
    private static Map<String, Network> networks = new HashMap<>();
    private static Map<String, Contract> contracts = new HashMap<>();
    public synchronized static void init(String channelName, String contractName) {

        if (getNetwork(channelName) == null)
            throw new RuntimeException("Could not create network");
        if (getContract(channelName, contractName) == null)
            throw new RuntimeException("Could not create contract");

    }

    public static Contract getContract(String channelName, String contractName) {
        if (contracts.get(channelName + "_" + contractName) == null)
            contracts.put(channelName + "_" + contractName, getNetwork(channelName).getContract(contractName)) ;
        return contracts.get(channelName + "_" + contractName);
    }

    public static Network getNetwork(String channelName) {
        if (networks.get(channelName) == null)
            networks.put(channelName, getGateway().getNetwork(channelName));
        return networks.get(channelName);
    }

    public synchronized static Gateway getGateway() {
        if (gateway == null) {
            try {
                boolean testnet = !Paths.get("connection-mainnet.json").toFile().exists();
                Path walletDirectory = testnet ? Paths.get("wallet-testnet") : Paths.get("wallet-mainnet");
                wallet = Wallets.newFileSystemWallet(walletDirectory);

                Identity appUser = HFUtils.getAppUser(wallet, testnet);

                // Path to a common connection profile describing the network.
                Path networkConfigFile = testnet ? Paths.get("connection-testnet.json") : Paths.get("connection-mainnet.json");

                // Configure the gateway connection used to access the network.
                org.hyperledger.fabric.gateway.Gateway.Builder builder = org.hyperledger.fabric.gateway.Gateway.createBuilder()
                        .identity(appUser)
                        .networkConfig(networkConfigFile);
                gateway = builder.connect();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (gateway == null)
            throw new RuntimeException("Could not create gateway");
        return gateway;
    }
}
